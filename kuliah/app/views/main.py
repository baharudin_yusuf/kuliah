from django.http.response import HttpResponse
from django.shortcuts import render


def index(request):
    context = {
        'title': 'Tugas Kuliah'
    }
    return render(request, "core.html", context)