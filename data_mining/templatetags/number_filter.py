from django import template

register = template.Library()


@register.filter(name='normalize_float')
def normalize_float(float_number):
    return float("{0:.3f}".format(float_number))
