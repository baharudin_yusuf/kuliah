import random

from helpers import find_centroid, euclidian_distance, load_data, load_original_data


def find_range(data):
    min = float("inf")
    max = 0

    for x in range(0, len(data)):
        for y in range(0, len(data[x])):
            if data[x][y] < min:
                min = data[x][y]
            if data[x][y] > max:
                max = data[x][y]

    return min, max


def k_means(filename, n_cluster):
    data = load_data(filename)
    original = load_original_data(filename)

    # inisialisasi cluster
    n_data = len(data)
    list_data = {}
    for x in range(0, n_data):
        origin_cluster = int(original[x][len(original[x])-1])
        origin_data = original[x]
        del origin_data[len(original[x])-1]

        list_data[x] = {
            'id': x,
            'data': data[x],
            'origin_data': original[x],
            'origin_cluster': origin_cluster
        }

    # random centroid
    centroid_member = {}
    min_num, max_num = find_range(data)
    n_dimension = len(data[0])
    for x in range(0, n_cluster):
        centroid_member[x] = {
            'centroid': [],
            'member': []
        }
        for y in range(0, n_dimension):
            centroid_member[x]['centroid'].append(random.uniform(min_num, max_num))


    max_loop = 2000
    n_loop = 0
    while True:
        for x in range(0, n_cluster):
            centroid_member[x]['member'] = []

        # hitung jarak data ke centroid terdekat
        for x in range(0, n_data):
            min_num = float("inf")
            cluster_id = None
            for y in range(0, n_cluster):
                dist = euclidian_distance(list_data[x]['data'], centroid_member[y]['centroid'])
                if dist < min_num:
                    cluster_id = y
                    min_num = dist
            centroid_member[cluster_id]['member'].append(list_data[x])

        n_sesuai = 0
        for x in range(0, n_cluster):
            if len(centroid_member[x]['member']) < 2:
                continue

            nc_data = []
            for item in centroid_member[x]['member']:
                nc_data.append(item['data'])
            nc = find_centroid(nc_data) # new centroid

            if nc == centroid_member[x]['centroid']:
                n_sesuai += 1

            # ubah data centroid baru
            centroid_member[x]['centroid'] = nc

        if n_sesuai == n_cluster:
            break

        if n_loop == max_loop:
            break
        n_loop += 1

    print "n loop = " + str(n_loop)
    return centroid_member
