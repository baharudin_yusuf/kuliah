from django.http.response import HttpResponse
from django.shortcuts import render
from .helpers import load_data
from .average_linkage import average_linkage as calc_average_linkage
from .centroid_linkage import centroid_linkage as calc_centroid_linkage
from .complete_linkage import complete_linkage as calc_complete_linkage
from .single_linkage import single_linkage as calc_single_linkage
from .k_means import k_means as calc_k_means
from data_mining.app.models.data import get_data
from .analysis import variance


def index(request):
    return HttpResponse("hahaha")


def average_linkage(request):
    data_list = get_data()
    file_selected = request.GET.get('file', '')
    n_cluster = 3

    try:
        n_cluster = data_list[file_selected]
    except KeyError:
        for item, n_data in data_list.items():
            file_selected = item
            n_cluster = n_data
            break

    cluster = calc_average_linkage(file_selected, n_cluster)
    context = {
        'title': "Average Linkage",
        'cluster': cluster,
        'a_variance': variance.analyze(cluster),
        'file_selected': file_selected,
        'data_list': data_list
    }
    return render(request, "data-mining/clustering/visualize.html", context)


def centroid_linkage(request):
    data_list = get_data()
    file_selected = request.GET.get('file', '')
    n_cluster = 3

    try:
        n_cluster = data_list[file_selected]
    except KeyError:
        for item, n_data in data_list.items():
            file_selected = item
            n_cluster = n_data
            break

    cluster = calc_centroid_linkage(file_selected, n_cluster)
    context = {
        'title': "Centroid Linkage",
        'cluster': cluster,
        'a_variance': variance.analyze(cluster),
        'file_selected': file_selected,
        'data_list': data_list
    }
    return render(request, "data-mining/clustering/visualize.html", context)


def complete_linkage(request):
    data_list = get_data()
    file_selected = request.GET.get('file', '')
    n_cluster = 3

    try:
        n_cluster = data_list[file_selected]
    except KeyError:
        for item, n_data in data_list.items():
            file_selected = item
            n_cluster = n_data
            break

    cluster = calc_complete_linkage(file_selected, n_cluster)
    context = {
        'title': "Complete Linkage",
        'cluster': cluster,
        'a_variance': variance.analyze(cluster),
        'file_selected': file_selected,
        'data_list': data_list
    }
    return render(request, "data-mining/clustering/visualize.html", context)


def single_linkage(request):
    data_list = get_data()
    file_selected = request.GET.get('file', '')
    n_cluster = 3

    try:
        n_cluster = data_list[file_selected]
    except KeyError:
        for item, n_data in data_list.items():
            file_selected = item
            n_cluster = n_data
            break

    cluster = calc_single_linkage(file_selected, n_cluster)
    context = {
        'title': "Single Linkage",
        'cluster': cluster,
        'a_variance': variance.analyze(cluster),
        'file_selected': file_selected,
        'data_list': data_list
    }
    return render(request, "data-mining/clustering/visualize.html", context)


def k_means(request):
    data_list = get_data()
    file_selected = request.GET.get('file', '')
    n_cluster = 3

    try:
        n_cluster = data_list[file_selected]
    except KeyError:
        for item, n_data in data_list.items():
            file_selected = item
            n_cluster = n_data
            break

    cluster = calc_k_means(file_selected, n_cluster)
    context = {
        'title': "K-Means",
        'cluster': cluster,
        'a_variance': variance.analyze(cluster),
        'file_selected': file_selected,
        'data_list': data_list
    }
    return render(request, "data-mining/clustering/visualize.html", context)
