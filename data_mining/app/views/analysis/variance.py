from data_mining.app.views.helpers import find_centroid


def get_vc2(cluster_data):
    vc = {}
    for cluster_id, data in cluster_data.items():
        centroid = data['centroid']

        sigma = float(0)
        for item in data['member']:
            temp = item['data']
            for x in range(0, len(temp)):
                temp[x] -= centroid[x]

            temp_result = float(0)
            for x in range(0, len(temp)):
                temp_result += pow(temp[x], 2)

            sigma += temp_result

        vc[cluster_id] = sigma / float(len(data['member']) - 1)

    return vc


def get_vw(cluster_data, vc2):
    vw = float(0)
    n_data = float(0)
    for cluster_id, data in cluster_data.items():
        vw += float(len(data['member']) - 1) * vc2[cluster_id]
        n_data += len(data['member'])

    return float(vw / n_data - len(cluster_data))


def get_vb(cluster_data):
    centroid_list = []
    for cluster_id, data in cluster_data.items():
        centroid_list.append(data['centroid'])
    main_centroid = find_centroid(centroid_list)

    sigma = float(0)
    for cluster_id, data in cluster_data.items():
        temp = data['centroid']
        for x in range(0, len(temp)):
            temp[x] -= main_centroid[x]

        temp_result = float(0)
        for x in range(0, len(temp)):
            temp_result += pow(temp[x], 2)

        sigma += float(len(data['member'])) * temp_result

    return sigma / float(len(cluster_data) - 1)


def analyze(cluster_data):
    vc2 = get_vc2(cluster_data)
    vw = get_vw(cluster_data, vc2)
    vb = get_vb(cluster_data)

    return float(vw / vb)