from random import randint


# normalkan float 3 angka dibelakang koma
def normalize_float(float_number):
    return float("{0:.3f}".format(float_number))


def euclidian_distance(t1, t2):
    if len(t1) > len(t2):
        for x in range(len(t2) - 1, len(t1)):
            t2.append(0)

    if len(t2) > len(t1):
        for x in range(len(t1) - 1, len(t2)):
            t1.append(0)

    # cari t
    t = []
    for z in range(0, len(t1)):
        t.append(float(t1[z] - t2[z]))

    # hitung d
    d = float(0)
    for a in range(0, len(t)):
        d += t[a] * t[a]

    return d


# normalisasi dengan menggunakan min max
def normalisasi(array):
    min = float("inf")
    max = float(0)
    new_min = float(0)
    new_max = float(100)

    for item in array:
        for value in item:
            if value < min:
                min = value
            if value > max:
                max = value

    result = []
    for item in array:
        temp = []
        for value in item:
            new_value = float((value - min) * (new_max - new_min) / (max - min) + new_min)
            temp.append(new_value)
        result.append(temp)

    return result


def find_centroid(arr):
    result = {}
    n_data = len(arr)

    for x in range(0, len(arr[0])):
        result[x] = float(0)

    for x in range(0, n_data):
        for y in range(0, len(arr[x])):
            result[y] += arr[x][y]

    for x in range(0, len(result)):
        result[x] /= float(n_data)

    return result


# load data dari file
def load_data(filename):
    file = "static/data/" + filename
    separator = " "
    result = []
    with open(file, "r") as ins:
        for line in ins:
            data = line.split(separator)
            temp = []
            for x in range(0, len(data) - 1):
                temp.append(float(data[x]))
            result.append(temp)

    return normalisasi(result)


def load_original_data(filename):
    file = "static/data/" + filename
    separator = " "
    result = []
    with open(file, "r") as ins:
        for line in ins:
            data = line.split(separator)
            temp = []
            for x in range(0, len(data)):
                temp.append(float(data[x]))
            result.append(temp)

    return result


# load random data
def load_random_data():
    data = []
    for x in range(0, 100):
        temp = []
        for y in range(0, 2):
            temp.append(randint(0, 100))
        data.append(temp)


def perkalian_matrix(m1, m2):
    result = []
    for x in range(0, len(m1)):
        temp = []
        for y in range(0, len(m1[x])):
            temp_num = float(0.0)
            for z in range(0, len(m2)):
                temp_num += m1[x][z] * m2[z][y]
            temp.append(temp_num)
        result.append(temp)
    return result