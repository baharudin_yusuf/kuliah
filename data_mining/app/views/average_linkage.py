from helpers import euclidian_distance, find_centroid, load_data, load_original_data


def find_distance(c1, c2):
    if c1 is None or c2 is None:
        return float("inf")

    sum_dist = 0
    for x in range(0, len(c1)):
        for y in range(0, len(c2)):
            dist = euclidian_distance(c1[x]['data'], c2[y]['data'])
            sum_dist += dist

    dist = float(sum_dist) / (float(len(c1)) * float(len(c2)))
    return dist


def average_linkage(filename, n_cluster):
    data = load_data(filename)
    original = load_original_data(filename)

    # inisialisasi cluster
    n_data = len(data)
    dy_n_data = n_data
    result_cluster = {}
    for x in range(0, n_data):
        origin_cluster = int(original[x][len(original[x])-1])
        origin_data = original[x]
        del origin_data[len(original[x])-1]

        result_cluster[x] = {
            'member': [{'id': x, 'data': data[x], 'origin_data': original[x], 'origin_cluster': origin_cluster}],
            'centroid': None
        }

    max_loop = 10000
    n_loop = 0
    removed = []
    while n_data > n_cluster:
        # hitung jarak antar cluster
        for x in result_cluster:
            min = float("inf")
            min_index = None
            for y in result_cluster:
                if x == y:
                    continue

                if x in removed or y in removed:
                    continue

                dist = find_distance(result_cluster[x]['member'], result_cluster[y]['member'])
                if dist < min:
                    min = dist
                    min_index = y

            if min_index is not None:
                for item in result_cluster[min_index]['member']:
                    result_cluster[x]['member'].append(item)
                removed.append(min_index)
                result_cluster[min_index]['member'] = None
                dy_n_data -= 1

            if dy_n_data <= n_cluster:
                break

        # hapus cluster yang sudah digabung
        for index in removed:
            try:
                del result_cluster[index]
            except KeyError:
                continue

        n_data = len(result_cluster)
        if n_loop > max_loop:
            break
        n_loop += 1

    # hitung centroid untuk tiap cluster
    for id, data in result_cluster.items():
        data_list = []
        for item in data['member']:
            data_list.append(item['data'])
        result_cluster[id]['centroid'] = find_centroid(data_list)

    return result_cluster
