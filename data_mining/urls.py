from django.conf.urls import url
from .app.views import main

urlpatterns = [
    url(r'^clustering/average-linkage/$', main.average_linkage, name='clustering-average-linkage'),
    url(r'^clustering/centroid-linkage/$', main.centroid_linkage, name='clustering-centroid-linkage'),
    url(r'^clustering/complete-linkage/$', main.complete_linkage, name='clustering-complete-linkage'),
    url(r'^clustering/single-linkage/$', main.single_linkage, name='clustering-single-linkage'),
    url(r'^clustering/k-means/$', main.k_means, name='clustering-k-means'),
]
