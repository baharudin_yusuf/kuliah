function selesaikanTransaksi(){
	$('#submit-button').click();
}

var submissionConfirmed = false;
$('#form-payment').submit(function(){
	prosesTransaksi();
})

function openDialog(url) {
	$.fancybox.open({
		href: url,
		type: 'iframe',
		autoSize: false,
		width: 400,
		height: 420,
		closeBtn: false,
		modal: true
	});
}
	
function closeDialog() {
	$.fancybox.close();
}

$(document).ready(function(){
	var deadline = $('#transaksi-deadline').val();
	$("#sisa-countdown").countdown(deadline, function(event) {
		$(this).text(
			event.strftime('%H Jam %M Menit %S Detik')
		);
	});
});