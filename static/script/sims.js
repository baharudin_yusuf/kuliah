function getKabupaten(){
    var provinsiId = $('#id_provinsi').val();
    ajaxTransfer(baseURL + '/backend/get-kabupaten/', {provinsi_id: provinsiId}, function(result){
        result = JSON.parse(result)
        $('#id_kabupaten').html('');

        for (var id in result) {
            if (result.hasOwnProperty(id)) {
                $('#id_kabupaten').append("<option value='"+id+"'>"+result[id]+"</option>");
            }
        }

        $('#id_kabupaten').trigger("chosen:updated");
        getKecamatan();
    })
}

function getKecamatan(){
    var kabupatenId = $('#id_kabupaten').val();
    ajaxTransfer(baseURL + '/backend/get-kecamatan/', {kabupaten_id: kabupatenId}, function(result){
        result = JSON.parse(result)
        $('#id_kecamatan').html('');

        for (var id in result) {
            if (result.hasOwnProperty(id)) {
                $('#id_kecamatan').append("<option value='"+id+"'>"+result[id]+"</option>");
            }
        }

        $('#id_kecamatan').trigger("chosen:updated")
    })
}

function removeUnusedOption(){
    var parent = $("option:contains('---------')").parent('select');
    $("option:contains('---------')").remove();
    $(parent).trigger("chosen:updated")
}

function scrollToTop(){
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
 }

function chevronActive(classname){
    $('.chevron-shapes li').removeClass('active');
    $('.chevron-shapes li.'+classname).addClass('active');
}

function isValidDate(dateString){
    // First check for the pattern
    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if(year < 1000 || year > 3000 || month == 0 || month > 12)
        return false;

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
};

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function setExpress(value){
    $('input[name=express_checkout]').val(value);
}

function getCsrfToken(){
    return $('input[name="csrfmiddlewaretoken"]').val();
}

function getExpress(){
    var express = $('input[name=express_checkout]').val();
    express = parseInt(express);
    if(isNaN(express)){
        express = 1;
    }

    return express;
}

function setPreview(){
    preRefinePreview();

    $('#tab-review-pesanan').html('');
    $('#form-pesanan').clone().appendTo('#tab-review-pesanan');

    $('#tab-review-pemesan').html('');
    $('#form-pemesan').clone().appendTo('#tab-review-pemesan');

    refinePreview();
}

function preRefinePreview(){
    var select = $('#form-pesanan select, #form-pesanan-paket select, #form-pemesan select');
    for(var i=0; i<select.length; i++){
        var name = $(select[i]).attr('name');
        var value = $(select[i]).val();

        $(select[i]).find('option[value='+value+']').attr('selected', 'selected');
        $(select[i]).find('option').not('[value='+value+']').removeAttr('selected');
    }

    var radio = $('#form-pesanan input[type=radio]:checked, #form-pesanan-paket input[type=radio]:checked, #form-pemesan input[type=radio]:checked');
    for(var i=0; i<radio.length; i++){
        $(radio[i]).parents('div.form-group').find('input[type=radio]').removeAttr('checked');
        $(radio[i]).prop('checked', true);
        $(radio[i]).attr('checked', 'checked');
    }
}

function refinePreview(){
    preRefinePreview();

    $('#output-preview').find('select, input, textarea, a, form')
        .attr('disabled', 'disabled')
        .removeAttr('id')
        .removeAttr('style')
        .removeAttr('onchange')
        .removeAttr('onkeyup')
        .removeAttr('onclick')
        .removeAttr('onsubmit')
        .removeAttr('name');
    $('#output-preview').find('.chosen-container, .remove-on-preview, input[type=hidden]').remove();

    var select = $('#output-preview select');
    for(var i=0; i<select.length; i++){
        var name = $(select[i]).attr('name');
        var value = $(select[i]).val();

        $(select[i]).find('option[value='+value+']').attr('selected', 'selected');
        $(select[i]).find('option').not('[value='+value+']').remove();
    }

    var input = $('#output-preview').find('input[type=text], input[type=number], input[type=email]');
    for(var i=0; i<input.length; i++){
        var value = $(input[i]).val();
        $(input[i]).attr('value', value);
    }

    var textarea = $('#output-preview').find('textarea');
    for(var i=0; i<textarea.length; i++){
        var value = $(textarea[i]).val();
        $(textarea[i]).html(value);
    }

    var checkbox = $('#output-preview').find('input[type=checkbox]:checked, input[type=radio]:checked');
    for(var i=0; i<checkbox.length; i++){
        $(checkbox[i]).attr('checked', 'checked');
    }

    postRefinePreview();
}

function postRefinePreview(){
    var input = $('#output-preview').find('input[type=text], input[type=number], input[type=email], textarea');
    for(var i=0; i<input.length; i++){
        var value = $(input[i]).val();
        if(value.length == 0){
            value = '-';
        }

        var siblingBefore = $(input[i]).prev('.input-group-addon').html();
        var siblingAfter = $(input[i]).next('.input-group-addon').html();
        if(typeof siblingBefore != 'undefined' && siblingBefore.length > 0){
            value = siblingBefore + ' : ' + value
        }
        if(typeof siblingAfter != 'undefined' && siblingAfter.length > 0){
            value = value + ' : ' + siblingAfter;
        }

        $(input[i]).parents('div.col-xs-12')
            .first()
            .addClass('form-control-static')
            .append(value+" ");
        $(input[i]).remove();
    }

    var select = $('#output-preview select');
    for(var i=0; i<select.length; i++){
        var value = $(select[i]).val();
        value = $(select[i]).find('option[value='+value+']').html();
        if(value.length == 0){
            value = '-';
        }
        $(select[i]).parents('div.col-xs-12')
            .first()
            .addClass('form-control-static')
            .append(value+" ");
        $(select[i]).remove();
    }

    var checkbox = $('#output-preview').find('input[type=checkbox]:checked, input[type=radio]:checked');
    for(var i=0; i<checkbox.length; i++){
        $(checkbox[i]).removeAttr('disabled').attr('onclick', 'return false;');
    }

    $('#output-preview .input-group, #output-preview .info').remove();
}

function logout(url){
    ajaxTransfer(url, {}, "#global-temp");
}

$(document).ready(function(){
    removeUnusedOption();
});